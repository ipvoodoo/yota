package ru.echodc.yota.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebView;
import butterknife.BindView;
import butterknife.ButterKnife;
import ru.echodc.yota.R;

public class ShowFullDetailsActivity extends AppCompatActivity {

  @BindView(R.id.web_view)
  WebView mWebView;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_show_full_details);
    Intent intent = getIntent();

    ButterKnife.bind(this);

    String link = intent.getStringExtra("article_link");

    mWebView.loadUrl(link);

    //    mWebView.setWebViewClient(new WebViewClient());
    //    mWebView.setWebChromeClient(new WebChromeClient());
    System.out.println(" ------------------- - - - - - -- - - - - - - - -- " + link.toString());
  }

  @Override
  public void onBackPressed() {
    Intent intent = new Intent(ShowFullDetailsActivity.this, MainActivity.class);

    //        Стартуем новую Активити
    startActivity(intent);

    //        Закрываем текущую Акстивити
    finish();

  }
}
