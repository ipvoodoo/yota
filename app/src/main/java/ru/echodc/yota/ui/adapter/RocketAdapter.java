package ru.echodc.yota.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.squareup.picasso.Picasso;
import java.util.List;
import ru.echodc.yota.R;
import ru.echodc.yota.data.MyData;
import ru.echodc.yota.ui.adapter.RocketAdapter.RocketViewHolder.CustomClickListener;

public class RocketAdapter extends RecyclerView.Adapter<RocketAdapter.RocketViewHolder> {

  private static final String TAG = "RocketAdapter";
  private Context mContext;
  private List<MyData> mRocketsList;

  private CustomClickListener mClickListener;

  public RocketAdapter(List<MyData> rocketsList, CustomClickListener customClickListener) {
    mRocketsList = rocketsList;
    this.mClickListener = customClickListener;
  }

  @Override
  public RocketViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    mContext = parent.getContext();
    View itemView = LayoutInflater.from(mContext).inflate(R.layout.item_rocket_list, parent, false);
    return new RocketViewHolder(itemView, mClickListener);
  }

  @Override
  public void onBindViewHolder(final RocketViewHolder holder, int position) {

    MyData item = (MyData) mRocketsList.get(position);
    //    Иям ракеты
    holder.mRocketNameTv.setText(item.getRocketName());
    //    Время запуска
    holder.mLaunchDateTv.setText(String.valueOf(item.getRocketLaunchTime()));
    //    Описание запуска
    holder.mDetailsTv.setText(item.getRocketDetails());
    //    Иконка ракеты в Айтеме
    Picasso.with(mContext)
        .load(item.getRocketImageIcon())
        .into(holder.mItemIconImg);
    /*    holder.mRocketNameTv.setText("РАКЕТА - !");
        holder.mLaunchDateTv.setText("Время запуска");
        holder.mDetailsTv.setText(R.string.lorem);
        holder.mItemIconImg.setImageResource(R.drawable.rocket_icon);*/
  }

  @Override
  public int getItemCount() {
    if (mRocketsList != null) {
      return mRocketsList.size();
    }
    return 0;
  }

  public static class RocketViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.rocket_name_tv)
    TextView mRocketNameTv;
    @BindView(R.id.launch_date_tv)
    TextView mLaunchDateTv;
    @BindView(R.id.details_tv)
    TextView mDetailsTv;
    @BindView(R.id.item_icon_img)
    ImageView mItemIconImg;

    @BindView(R.id.item_rel_layout)
    RelativeLayout mItemRelLayout;

    private CustomClickListener mListener;

    public RocketViewHolder(View itemView, CustomClickListener customClickListener) {
      super(itemView);
      ButterKnife.bind(this, itemView);
      mListener = customClickListener;
    }

    //    Кастомный слушатель для переброса в Активити
    public interface CustomClickListener {

      void onItemClickListener(int position);
    }
    @OnClick(R.id.item_rel_layout)
    void itemClick() {
      if (mListener != null) {
        mListener.onItemClickListener(getAdapterPosition());
      }
    }
  }

  @Override
  public void onAttachedToRecyclerView(RecyclerView recyclerView) {
    super.onAttachedToRecyclerView(recyclerView);
  }
}
