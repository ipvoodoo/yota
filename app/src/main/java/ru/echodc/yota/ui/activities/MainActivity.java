package ru.echodc.yota.ui.activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.net.ssl.HttpsURLConnection;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import ru.echodc.yota.R;
import ru.echodc.yota.data.MyData;
import ru.echodc.yota.ui.adapter.RocketAdapter;
import ru.echodc.yota.ui.adapter.RocketAdapter.RocketViewHolder.CustomClickListener;
import ru.echodc.yota.utils.AppConfig;

public class MainActivity extends AppCompatActivity {

  @BindView(R.id.click_to_start)
  TextView mClickToStart;

  @BindView(R.id.rocket_big_img)
  ImageView mRocketBigImg;

  @BindView(R.id.rockets_list)
  RecyclerView mRecyclerView;

  private List<MyData> rocketsList;

  Intent intent;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    ButterKnife.bind(this);

    if (android.os.Build.VERSION.SDK_INT > 9) {
      StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
      StrictMode.setThreadPolicy(policy);
    }
  }

  // region ===================== Local =====================
  //  Грузим данные из файла в ресурсах
  private void addItemsFromJson() {
    rocketsList = new ArrayList<>();
    try {
      String jsonDataString = readJsonDataStringFromFile();
      JSONArray itemsJsonArray = new JSONArray(jsonDataString);

      for (int i = 0; i < itemsJsonArray.length(); ++i) {
        JSONObject itemObject = itemsJsonArray.getJSONObject(i);

        //        Получаем имя ракеты
        String itemRocketName = itemObject.getJSONObject("rocket").getString("rocket_name");
        //        Поулчаем и сразу конвертируем время запуска
        String itemRocketLaunchTime = convertLaunchDate(itemObject.getString("launch_date_unix"));
        //        Получаем детели запуска
        String itemRocketDetails = itemObject.getString("details");
        //        Получаем иконку ракеты
        String itemRocketIcon = itemObject.getJSONObject("links").getString("mission_patch");
        //          Получаем ссылку на полное описание
        String itemRocketArticleLink = itemObject.getJSONObject("links").getString("article_link");

        MyData data = new MyData(itemRocketName, itemRocketLaunchTime, itemRocketDetails, itemRocketIcon, itemRocketArticleLink);

        System.out.println("====           " + data.getRocketName().toString());
        System.out.println("====           " + data.getRocketLaunchTime().toString());
        System.out.println("====           " + data.getRocketDetails().toString());
        System.out.println("====           " + data.getRocketImageIcon().toString());

        rocketsList.add(data);
      }
      //      Инициализируем Ресайклер
      setUpRecycler();

    } catch (JSONException e) {
      e.printStackTrace();
      Log.d(MainActivity.class.getName(), "Не получилось распарсить Json!", e);
      Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_LONG).show();
    } catch (IOException e) {
      e.printStackTrace();
      Log.d(MainActivity.class.getName(), "IOException", e);
      Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_LONG).show();
    }
  }

  private String readJsonDataStringFromFile() throws IOException {

    InputStream inputStream = null;
    StringBuilder builder = new StringBuilder();
    try {
      String jsonDataString;
      inputStream = getResources().openRawResource(R.raw.item);
      BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
      while ((jsonDataString = bufferedReader.readLine()) != null) {
        builder.append(jsonDataString);
      }

    } finally {
      if (inputStream != null) {
        inputStream.close();
      }
    }

    return new String(builder);
  }

  // endregion Local
  // region ===================== Network =====================
  @SuppressLint("StaticFieldLeak")
  public class LoadDataFromServer extends AsyncTask<String, String, String> {

    HttpsURLConnection mConnection;
    URL mURL;
    ProgressDialog mProgressDialog = new ProgressDialog(MainActivity.this);

    @Override
    protected void onPreExecute() {
      super.onPreExecute();

      mProgressDialog.setMessage("Пробую загрузить данные с сервера!");
      mProgressDialog.setCancelable(false);
      mProgressDialog.show();
    }

    @Override
    protected String doInBackground(String... params) {

      try {
        //          Передаем адрес подключения
        mURL = new URL(AppConfig.BASE_URL);
      } catch (MalformedURLException e) {
        e.printStackTrace();
        return e.toString();
      }

      try {
        //          Устанавливаем подключение
        mConnection = (HttpsURLConnection) mURL.openConnection();
        mConnection.setReadTimeout(AppConfig.MAX_READ_TIMEOUT);
        mConnection.setConnectTimeout(AppConfig.MAX_CONNECT_TIMEOUT);
        mConnection.setRequestMethod("GET");
        mConnection.connect();

      } catch (IOException e) {
        e.printStackTrace();
        e.toString();
      }

      try {
        int responseCode = mConnection.getResponseCode();

        if (responseCode == HttpsURLConnection.HTTP_OK) {
          //          Проверяем код успешного подключения
          InputStream inputStream = mConnection.getInputStream();
          BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
          StringBuilder result = new StringBuilder();
          String line;

          while ((line = reader.readLine()) != null) {
            result.append(line);
          }

          //          Передаем возвращенные данные в onPostExecute метод
          return result.toString();
        } else {
          return ("Произошла дичь!");
        }

      } catch (IOException e) {
        e.printStackTrace();
        return e.toString();
      } finally {
        mConnection.disconnect();
      }
    }

    @Override
    protected void onPostExecute(String result) {

      mProgressDialog.dismiss();

      Toast.makeText(getApplicationContext(), "Данные успешно получены!", Toast.LENGTH_SHORT).show();

      //      Создаем новый массив для полученных данных
      rocketsList = new ArrayList<>();

      try {
        JSONArray itemsJsonArray = new JSONArray(result);

        for (int i = 0; i < itemsJsonArray.length(); i++) {
          JSONObject jsonData = itemsJsonArray.getJSONObject(i);

          //        Получаем имя ракеты
          String itemRocketName = jsonData.getJSONObject("rocket").getString("rocket_name");
          //        Поулчаем и сразу конвертируем время запуска
          String itemRocketLaunchTime = convertLaunchDate(jsonData.getString("launch_date_unix"));
          //        Получаем детели запуска
          String itemRocketDetails = jsonData.getString("details");
          //        Получаем иконку ракеты
          String itemRocketIcon = jsonData.getJSONObject("links").getString("mission_patch");
          //          Получаем ссылку на полное описание
          String itemRocketArticleLink = jsonData.getJSONObject("links").getString("article_link");

          //            Передаем все, полученные данные в конструктор
          MyData data = new MyData(itemRocketName, itemRocketLaunchTime, itemRocketDetails, itemRocketIcon, itemRocketArticleLink);

          System.out.println("====           " + data.getRocketName().toString());
          System.out.println("====           " + data.getRocketLaunchTime().toString());
          System.out.println("====           " + data.getRocketDetails().toString());
          System.out.println("====           " + data.getRocketImageIcon().toString());
          System.out.println("====           " + data.getRocketArticleLink().toString());

          //        Добавляем данные в список
          rocketsList.add(data);

          intent = new Intent(MainActivity.this, ShowFullDetailsActivity.class);
          intent.putExtra("article_link", itemRocketArticleLink);
          System.out.println(" ************************************   " + itemRocketArticleLink);

        }

        //      Инициализируем Ресайклер
        setUpRecycler();

      } catch (JSONException e) {
        e.printStackTrace();
        Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_LONG).show();
      }
    }
  }

  // endregion Network

  // region ===================== Events =====================
  @OnClick(R.id.rocket_big_img)
  public void rocketClick() {
    mClickToStart.setVisibility(View.GONE);
    mRocketBigImg.setVisibility(View.GONE);

    //    Даные из сети
    new LoadDataFromServer().execute();

    //    Данные из файла
    //    addItemsFromJson();
  }

  @Override
  public void onBackPressed() {
    mRecyclerView.setVisibility(View.GONE);

    mClickToStart.setVisibility(View.VISIBLE);
    mRocketBigImg.setVisibility(View.VISIBLE);

    super.onBackPressed();
  }

  // endregion Events

  // region ===================== OthersMethods =====================
  //    Конвертируем время в человеческий вид
  private String convertLaunchDate(String string) {
    long batch_date = Long.parseLong(string);
    Date dt = new Date(batch_date * 1000);

    SimpleDateFormat sfd = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

    sfd.format(dt);
    return sfd.format(dt);
  }

  private void setUpRecycler() {
    LinearLayoutManager layoutManager = new LinearLayoutManager(this);
    mRecyclerView.setHasFixedSize(true);
    mRecyclerView.setLayoutManager(layoutManager);

    final RocketAdapter rocketAdapter = new RocketAdapter(rocketsList, new CustomClickListener() {
      @Override
      public void onItemClickListener(int position) {
//        rocketsList.get(position).getRocketArticleLink().toString();

        //        Стартуем новую Активити
        startActivity(intent);

        //        Закрываем текущую Акстивити
        //        finish();
      }
    });

    mRecyclerView.setAdapter(rocketAdapter);

    rocketAdapter.notifyDataSetChanged();
  }

  // endregion OthersMethods

}
