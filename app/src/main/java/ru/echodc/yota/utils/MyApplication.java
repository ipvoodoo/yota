package ru.echodc.yota.utils;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class MyApplication extends Application {
  private static Context sContext;
  private static SharedPreferences sSharedPreferences;

  public static Context getContext() {
    return sContext;
  }

  public static SharedPreferences getSharedPreferences() {
    return sSharedPreferences;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    sContext = getApplicationContext();
    sSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
  }
}
