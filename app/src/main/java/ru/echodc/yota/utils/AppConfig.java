package ru.echodc.yota.utils;

public interface AppConfig {

  String BASE_URL = "https://api.spacexdata.com/v2/launches?launch_year=2017";

  int MAX_READ_TIMEOUT = 3000;
  int MAX_CONNECT_TIMEOUT = 3000;
  int START_DELAY = 1500;
  int SEARCH_DELAY = 3000;
}

