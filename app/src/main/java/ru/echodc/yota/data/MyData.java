package ru.echodc.yota.data;

public class MyData {

  private String mRocketName;
  private String mRocketLaunchTime;
  private String mRocketDetails;
  private String mRocketImageIcon;

  private String mRocketArticleLink;

  public MyData(String rocketName, String rocketLaunchTime, String rocketDescription, String rocketImageIcon, String rocketArticleLink) {
    this.mRocketName = rocketName;
    this.mRocketLaunchTime = rocketLaunchTime;
    this.mRocketDetails = rocketDescription;
    this.mRocketImageIcon = rocketImageIcon;
    this.mRocketArticleLink = rocketArticleLink;
  }

  public String getRocketName() {
    return mRocketName;
  }


  public String getRocketLaunchTime() {
    return mRocketLaunchTime;
  }


  public String getRocketDetails() {
    return mRocketDetails;
  }


  public String getRocketImageIcon() {
    return mRocketImageIcon;
  }

  public String getRocketArticleLink() {
    return mRocketArticleLink;
  }
}
